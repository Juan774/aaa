package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import modelo.Alumno;
import modelo.Conexion;


public class AlumnoDAO {
	private Conexion con;
	private Connection connection;

	public AlumnoDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) throws SQLException {
		System.out.println(jdbcURL);
		con = new Conexion(jdbcURL, jdbcUsername, jdbcPassword);
	}

	// insertar alumno
	public boolean insertar(Alumno alumnos) throws SQLException {
		String sql = "INSERT INTO alumno (id, apellnombres, correo, telefono) VALUES (?, ?, ?,?)";
		System.out.println(alumnos.getApellnombres());
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, null);
		statement.setString(2, alumnos.getApellnombres());
		statement.setString(3, alumnos.getCorreo());
		statement.setString(4, alumnos.getTelefono());


		boolean rowInserted = statement.executeUpdate() > 0;
		statement.close();
		con.desconectar();
		return rowInserted;
	}


	public List<Alumno> listaAlumnos() throws SQLException {

		List<Alumno> listaAlumnos = new ArrayList<Alumno>();
		String sql = "SELECT * FROM alumno";
		con.conectar();
		connection = con.getJdbcConnection();
		Statement statement = connection.createStatement();
		ResultSet resulSet = statement.executeQuery(sql);

		while (resulSet.next()) {
			int id = resulSet.getInt("id");
			String apellnombres = resulSet.getString("apellnombres");
			String curso = resulSet.getString("correo");
			String telefono = resulSet.getString("telefono");
		    Alumno alumno = new Alumno(id,apellnombres,curso,telefono);
		    listaAlumnos.add(alumno);
		}
		con.desconectar();
		return listaAlumnos;
	}

	// obtener por id
	public Alumno obtenerPorId(int id) throws SQLException {
		Alumno alumno = null;

		String sql = "SELECT * FROM articulos WHERE id= ? ";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, id);

		ResultSet res = statement.executeQuery();
		if (res.next()) {
			alumno = new Alumno(res.getInt("id"), res.getString("apellnombres"), res.getString("correo"),
					res.getString("telefono"));
		}
		res.close();
		con.desconectar();

		return alumno;
	}

	// actualizar
	public boolean actualizar(Alumno alumno) throws SQLException {
		boolean rowActualizar = false;
		String sql = "UPDATE alumno SET apellnombre=?,correo=?,telefono=? WHERE id=?";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setString(1, alumno.getApellnombres());
		statement.setString(2, alumno.getCorreo());
		statement.setString(3, alumno.getTelefono());

		rowActualizar = statement.executeUpdate() > 0;
		statement.close();
		con.desconectar();
		return rowActualizar;
	}
	
	//eliminar
	public boolean eliminar(Alumno articulo) throws SQLException {
		boolean rowEliminar = false;
		String sql = "DELETE FROM alumno WHERE ID=?";
		con.conectar();
		connection = con.getJdbcConnection();
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, articulo.getIdalumno());

		rowEliminar = statement.executeUpdate() > 0;
		statement.close();
		con.desconectar();

		return rowEliminar;
	}

}
