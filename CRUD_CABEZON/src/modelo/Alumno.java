package modelo;

public class Alumno {
	private int idalumno;
	private String apellnombres;
	private String correo;
	private String telefono;
	
	public Alumno() {
	}
	
	public Alumno(int idalumno, String apellnombres, String correo, String telefono) {
		super();
		this.idalumno = idalumno;
		this.apellnombres = apellnombres;
		this.correo = correo;
		this.telefono = telefono;
	}

	
	public int getIdalumno() {
		return idalumno;
	}
	public void setIdalumno(int idalumno) {
		this.idalumno = idalumno;
	}
	public String getApellnombres() {
		return apellnombres;
	}
	public void setApellnombres(String apellnombres) {
		this.apellnombres = apellnombres;
	}
	public String getCorreo() {
		return correo;
	}
	public void getCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	@Override
	public String toString() {
		return this.idalumno+", "+this.apellnombres+", "+this.correo+", "+this.telefono;
	}

}
