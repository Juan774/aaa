<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Alumnos</h1>
	<table>
		<tr>
			<td><a href="adminAlumno?action=index" >Ir al men�</a> </td>
		</tr>
	</table>
	
	<table border="1" width="100%">
		<tr>
		 <td> # </td>
		 <td> Apellidos y Nombres</td>
		 <td> Correo</td>
		 <td>Telefono</td>
		
		 <td colspan=2>ACCIONES</td>
		</tr>
		<c:forEach var="alumno" items="${lista}">
			<tr>
				<td><c:out value="${alumno.idalumno}"/></td>
				<td><c:out value="${alumno.apellnombres}"/></td>
				<td><c:out value="${alumno.correo}"/></td>
				<td><c:out value="${alumno.telefono}"/></td>

				<td><a href="adminAlumno?action=showedit&id=<c:out value="${alumno.idalumno}" />">Editar</a></td>
				<td><a href="adminAlumno?action=eliminar&id=<c:out value="${alumno.idalumno}"/>">Eliminar</a> </td>				
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>