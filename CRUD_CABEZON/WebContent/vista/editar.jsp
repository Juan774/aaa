<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Actualizar Alumno</h1>
	<form action="adminAlumno?action=editar" method="post" >
		<table>
			<tr>
				<td><label>Id</label></td>
				<td><input type="text" name="idalumno" value="<c:out value="${alumno.idalumno}"></c:out>" ></td>
			</tr>
			<tr>
				<td><label>Apellidos y nombres</label></td>
				<td><input type="text" name="apellnombres" value='<c:out value="${alumno.apellnombres}"></c:out>' ></td>
			</tr>
			<tr>
				<td><label>Correo</label></td>
				<td><input type="text" name="correo" value='<c:out value="${alumno.correo}"></c:out>' ></td>
			</tr>
			<tr>
				<td><label>Telefono</label></td>
				<td><input type="text" name="telefono" value='<c:out value="${alumno.telefono}"></c:out>' ></td>
			</tr>
			
	
		<input type="submit" name="registrar" value="Guardar"> 
	</form>
</body>
</html>