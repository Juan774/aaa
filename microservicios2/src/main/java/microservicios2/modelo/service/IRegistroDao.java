package microservicios2.modelo.service;

import java.util.List;
import microservicios2.modelo.entity.Registro;
public interface IRegistroDao {
    public List<Registro> readAll();
    public Registro readBy(Long id);
    
}
